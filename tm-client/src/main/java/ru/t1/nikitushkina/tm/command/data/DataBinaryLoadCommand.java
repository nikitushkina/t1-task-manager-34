package ru.t1.nikitushkina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.DataBinaryLoadRequest;
import ru.t1.nikitushkina.tm.enumerated.Role;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        getDomainEndpointClient().loadDataBinary(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
