package ru.t1.nikitushkina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.endpoint.IDomainEndpoint;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @NotNull
    protected IDomainEndpoint getDomainEndpointClient() {
        return serviceLocator.getDomainEndpointClient();
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
