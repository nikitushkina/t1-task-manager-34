package ru.t1.nikitushkina.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.User;

public class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable User user) {
        super(user);
    }

}
