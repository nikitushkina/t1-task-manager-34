package ru.t1.nikitushkina.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(@Nullable String token) {
        super(token);
    }

}
