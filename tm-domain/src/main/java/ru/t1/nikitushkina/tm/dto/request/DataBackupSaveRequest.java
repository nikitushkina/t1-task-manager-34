package ru.t1.nikitushkina.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class DataBackupSaveRequest extends AbstractUserRequest {

    public DataBackupSaveRequest(@Nullable String token) {
        super(token);
    }

}
