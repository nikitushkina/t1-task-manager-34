package ru.t1.nikitushkina.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataBackupSaveResponse extends AbstractResponse {
}
