package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class TaskGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String Id;

    public TaskGetByIdRequest(@Nullable String token) {
        super(token);
    }

}
