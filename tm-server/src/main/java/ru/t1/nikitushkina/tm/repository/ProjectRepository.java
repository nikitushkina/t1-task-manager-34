package ru.t1.nikitushkina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.IProjectRepository;
import ru.t1.nikitushkina.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
