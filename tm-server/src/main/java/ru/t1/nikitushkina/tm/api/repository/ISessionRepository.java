package ru.t1.nikitushkina.tm.api.repository;

import ru.t1.nikitushkina.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
