package ru.t1.nikitushkina.tm.repository;

import ru.t1.nikitushkina.tm.api.repository.ISessionRepository;
import ru.t1.nikitushkina.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
